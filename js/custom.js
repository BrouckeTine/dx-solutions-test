$(document).ready(function () {
    script();
});

function script() {
    //sidebar
    $('[data-toggle=offcanvas]').click(function () {
        $('.row-offcanvas').toggleClass('active');
        $('.navMin a i').toggleClass('glyphicon-chevron-right').toggleClass('glyphicon-chevron-left');
        $('#menu .collapse').toggleClass('in').toggleClass('hidden-xs').toggleClass('visible-xs');
        
       });
    
    $('.panel-title a').click(function () {
        var id = $(this).attr('href').split('#')[1];
        $('#accordion  .collapse').each(function () {
            if ($(this).hasClass('in')) {
                $(this).removeClass('in');
            }
        });
        
        $("#"+id).addClass('in');
    });
   

    $("#sidebar ul li").click(function () {

        $("#sidebar ul li").click(function () {
            $("#sidebar ul li").removeClass("active");
            $(this).addClass("active");
        });
        
    });

    $('.pay').click(function () {
        $("html, body").animate({ scrollTop: 0 }, 100);
        $(".nextf").trigger('click');
        $('.nextf span').removeClass("inactive");
        $('.two span').removeClass('active');
        $('.two span').addClass('ok');
        $('.two').css("pointer-events", "none");
        $('.nextf span').addClass("active");
      

    })

    var height = $('.wrapper').height();
    $('#sidebar').css("height", height+"px");
    //var $select = $(".amount");
    //for (i = 0; i <= 40; i++) {
    //    $select.append($('<option></option>').val(i).html(i))
    //}
    //$(".pay").click(function () {
        
    //    $('.last').show();
    //    $('.home').hide();

    //});
    
    $(".select").select2();


    $('td button').on("click", function (e) {
        var amount = $(this).closest('.product').find('.amount');
        var replace = $(this).parent().closest('.product').find('td button i');
        var button = $(this).closest('.product').find('td button');
        var test = button.text();
        //adjust cart
        $(".count").animate({ width: 30, height: 30 }, "fast");
        $(".count").animate({ width: 20, height: 20 }, "fast");
        $(".number").animate({ "font-size": 25 }, "fast");
        $(".number").animate({ "font-size": 14}, "fast");

        if (button.text() == "besteld") {
            button.text("");
            button.append('<i class="glyphicon glyphicon-shopping-cart"></i>');
            amount.val(0);

            button.css("background-color", "#9B9B9B");
        } 
        if (amount.val() != "0") {

            replace.removeClass('glyphicon glyphicon-shopping-cart');
            replace.parent().text("besteld");
            button.css("background-color", "#ddd");
        }
    });
    $(".amount").on('change', function () {
        var amount = $(this).val();
        var button = $(this).closest('.product').find('td button');
        if (amount == "0") {
            button.text("");
            button.removeClass("ordered");
            button.append('<i class="glyphicon glyphicon-shopping-cart"></i>')
            //button.css("background-color", "#9B9B9B");

        }


    })

  (function () {
      var $frame = $('#forcecentered');
      var $wrap = $frame.parent();

      // Call Sly on frame
      $frame.sly({
          horizontal: 1,
          itemNav: 'forceCentered',
          smart: 1,
          activateMiddle: 1,
          activateOn: 'click',
          mouseDragging: 1,
          touchDragging: 1,
          releaseSwing: 1,
          startAt: 0,
          scrollBar: $wrap.find('.scrollbar'),
          scrollBy: 1,
          speed: 300,
          elasticBounds: 1,
          easing: 'easeOutExpo',
          dragHandle: 1,
          dynamicHandle: 1,
          clickBar: 1,

          // Buttons
          prev: $wrap.find('.prev'),
          next: $wrap.find('.next')
      });
  }());
};